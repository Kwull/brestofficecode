﻿using System;
using System.Threading;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region stubs
            object coffee = 2;
            bool asleep = true;
            object football = 1;
            object kicker = 1;
            object tennis = 2;
            object voleyball = 3;
            object tea = 1;
            var interest = Interest.Sport;
            object beer = 6;
            object xbox = 7;
            #endregion
            // --------- START ---------- 

            try {
                EPAM_Brest:

                do {
                    WorkOnTheProject();

                    if (NeedAdditionalKnowledge()) {
                        Browse("msdn.com");
                        AttendEnglishCourses();
                        ParticipateInCareerDevelopmentProgram();
                        GoToTraining();
                        GoToTechTalk();
                    }

                    Drink(DateTime.Now.Hour < 12 ? coffee : tea);
                    Play(kicker);
                    Play(tennis);
                    Play(xbox);
                    GoToSportGym();

                    if (DateTime.Now.Hour == 13) {
                        WatchMovieInEnglish();
                        EatSomething();
                        Browse("youtube.com");
                        Browse("habrahabr.ru");
                    }

                } while (DateTime.Now.Hour <= 18);

                do {
                    switch (interest) {
                        case Interest.Sport:
                            Play(football);
                            Play(voleyball);
                            break;
                        case Interest.Social:
                            Drink(beer);
                            break;
                        default:
                            HaveRest();
                            break;
                    }
                } while (!asleep);

                Thread.Sleep(8 * 60 * 60 * 1000);
            }
            catch (MarriageException) {
                GetCompensation(500); 
            }
            catch (ChildBirthException) {
                GetCompensation(500);
            }
            catch (HealthException ex) {
                SeeTheDoctorAtLODE(ex);
            }

            // --------- END ---------- 
        }

        #region stubs

        private static void AttendEnglishCourses()
        {
            throw new NotImplementedException();
        }

        private static void Browse(string youtubeCom)
        {
            throw new NotImplementedException();
        }

        private static void EatSomething()
        {
            throw new NotImplementedException();
        }

        private static void WatchMovieInEnglish()
        {
            throw new NotImplementedException();
        }

        public enum Interest
        {
            Sport, Social, 
        }

        private static void Drink(object coffee)
        {
            throw new NotImplementedException();
        }

        private static void Play(object football)
        {
            throw new NotImplementedException();
        }

        private static void WorkOnTheProject()
        {
            throw new NotImplementedException();
        }

        private static void HaveRest()
        {
            throw new NotImplementedException();
        }

        private static void GoToTechTalk()
        {
            throw new NotImplementedException();
        }

        private static void GoToTraining()
        {
            throw new NotImplementedException();
        }

        private static void ParticipateInCareerDevelopmentProgram()
        {
            throw new NotImplementedException();
        }

        private static bool NeedAdditionalKnowledge()
        {
            throw new NotImplementedException();
        }

        private static void GetCompensation(int i)
        {
            throw new NotImplementedException();
        }

        private static void SeeTheDoctorAtLODE(HealthException healthException)
        {
            throw new NotImplementedException();
        }

        private static void GoToSportGym()
        {
            throw new NotImplementedException();
        }

        internal class ChildBirthException : Exception
        {
        }

        internal class MarriageException : Exception
        {
        }

        internal class HealthException : Exception
        {
        }
        #endregion 
    }
 }
